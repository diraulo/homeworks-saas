class Movie < ActiveRecord::Base

  # Returns all possible values for movie ratings
  def self.all_ratings
    self.uniq.pluck(:rating)
  end
end
