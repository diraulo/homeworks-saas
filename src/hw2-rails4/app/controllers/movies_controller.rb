class MoviesController < ApplicationController
  def index
    # Rafactored to keep state of checked ratings
    # mainly inspired by Alberto G. Jácome solution
    # https://github.com/agjacome/saas1_hw2.git

    @all_ratings = Movie.all_ratings

    redirect = false

    if params[:sort_by]
      @sort_by = params[:sort_by]
    elsif session[:sort_by]
      @sort_by = session[:sort_by]
      redirect = true
    end

    if params[:ratings]
      @ratings = params[:ratings]
    elsif session[:ratings]
      @ratings = session[:ratings]
      redirect = true
    else
      @all_ratings.each do |rat|
        (@ratings ||= { })[rat] = 1
      end
      redirect = true
    end

    if redirect
      redirect_to movies_path(:sort_by => @sort_by, :ratings => @ratings)
    end

    Movie.all.order(@sort_by ? @sort_by : :id).each do |mv|
      if @ratings.keys.include? mv[:rating]
        (@movies ||= [ ]) << mv
      end
    end

    session[:sort_by] = @sort_by
    session[:ratings] = @ratings
  end

  def show
    id = params[:id]  # retrieve movie ID from URI route
    @movie = Movie.find(id)  # look up movie by unique ID
  end

  def new
    # default: render 'new' template
  end

  def create
    @movie = Movie.create!(movie_params)
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movie_path(@movie)
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(movie_params)
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find params[:id]
    @movie.destroy
    flash[:notice] = "#{@movie.title} deleted"
    redirect_to movies_path
  end

  private
    def movie_params
      params.require(:movie).permit(:title, :rating, :description, :release_date)
    end

end
