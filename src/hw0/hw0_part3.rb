# Define a class BookInStock which represents a book with an isbn number, isbn,
# and price of the book as a floating-point number, price, as attributes.
# The constructor should accept the ISBN number (a string) as the first argument
# and price as second argument, and should raise ArgumentError (one of Ruby's built-in exception types)
# if the ISBN number is the empty string or if the price is less than or equal to zero.

# Include the proper getters and setters for these attributes.
# Include a method price_as_string that returns the price of the book with a leading
# dollar sign and trailing zeros, that is, a price of 20 should display as "$20.00"
# and a price of 33.8 should display as "$33.80".

class BookInStock
  def initialize(isbn, price)
    raise ArgumentError, "Provide valide ISBN number and Price must be greater than 0" if isbn.empty? || price <= 0
    @isbn = isbn
    @price = price
  end

  attr_accessor :isbn, :price

  def price_as_string
    p = "$#{@price.round(3)}"
    return p =~ /^[$][0-9]+\.\d$/ ? "#{p}0" : p
  end
end

# book1 = BookInStock.new("111222", 20)
# book2 = BookInStock.new("111223", 33.8)
# book3 = BookInStock.new("111224", 112.22)

# raise "Should return price of $20.00"  unless book1.price_as_string == "$20.00"
# raise "Should return price of $33.80"  unless book2.price_as_string == "$33.80"
# raise "Should return price of $112.22" unless book3.price_as_string == "$112.22"
