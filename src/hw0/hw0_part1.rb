# Task 1
# Define a method sum which takes an array of integers as an argument and
# returns the sum of its elements. For an empty array it should return zero.
def sum(array)
  return 0 if array.empty?
  array.inject {|sum, n| sum + n }
end

raise "sum([]) is not 0" if sum([]) != 0
raise "sum([1,2,3,4]) should be 10" if sum([1,2,3,4]) != 10
raise "sum([1,2,3,4,5]) should be 15" if sum([1,2,3,4,5]) != 15


# Define a method max_2_sum which takes an array of integers as an argument and
# returns the sum of its two largest elements. For an empty array it should
# return zero. For an array with just one element, it should return that element.

def max_2_sum(array)
  return 0 if array.empty?
  return array[0] if array.length == 1

  max_1 = array.max
  array.delete_at(array.index(max_1))
  max_2 = array.max
  max_1 + max_2
end

raise "sum([]) is not 0" if max_2_sum([]) != 0
raise "sum([1]) should be 1" if max_2_sum([1]) != 1
raise "sum([1,2,3,4,5]) should be 9" if max_2_sum([1,2,3,4,5]) != 9
raise "sum([1,2,3,4,4]) should be 8" if max_2_sum([1,2,3,4,4]) != 8

# Define a method sum_to_n? which takes an array of integers and an additional
# integer, n, as arguments and returns true if any two distinct elements in the
# array of integers sum to n. An empty array or single element array
# should both return false.

def sum_to_n?(array, num)
  return false if array.empty? or array.length == 1

  array.each_index do |n|
    array.each_index do |p|
      unless n == p
        sum = array[n] + array[p]
        return true if sum == num
      end
    end
  end

  return false
end


raise "sum([], 2) should return false" if sum_to_n?([], 2) != false
raise "sum([1], 2) should return false" if sum_to_n?([1], 2) != false
raise "sum([1,2,3,4,5], 7) should return true" if sum_to_n?([1,2,3,4,5], 7) != true
raise "sum([1,2,3,4,4], 20) should return false" if sum_to_n?([1,2,3,4,4], 20) != false
