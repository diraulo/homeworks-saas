# Define a method hello(name) that takes a string representing
# a name and returns the string "Hello, " concatenated with the name.
def hello(name)
  return "Hello, #{name}"
end

=begin
raise "should return 'Hello, Raoul'" unless hello("Raoul") == "Hello, Raoul"
raise "should return 'Hello, Diogo'" unless hello("Diogo") == "Hello, Diogo"
=end

# Define a method starts_with_consonant?(s) that takes a string
# and returns true if it starts with a consonant and false otherwise.
# (For our purposes, a consonant is any letter other than A, E, I, O, U.)
# NOTE: be sure it works for both upper and lower case and for nonletters!

def starts_with_consonant?(s)
  return true unless (s[0] =~ /\A(?=[^aeiou])(?=[a-z])/i).nil?
  false
end
=begin
raise "should return 'false' if any number is given" if starts_with_consonant?("123456789")
raise "should return 'false' if empty string" if starts_with_consonant?("")
raise "should return 'false' if starts with lowercase vowel" if starts_with_consonant?("anna")
raise "should return 'false' if starts with uppercase vowel" if starts_with_consonant?("Anna")

raise "should return 'true' if starts with consonant uppercase" if !starts_with_consonant?("Raoul")
raise "should return 'true' if starts with consonant lowercase" if !starts_with_consonant?("diogo")
=end

# Define a method binary_multiple_of_4?(s) that takes a string
# and returns true if the string represents a binary number that is a multiple of 4.
# NOTE: be sure it returns false if the string is not a valid binary number!

def binary_multiple_of_4?(s)
  return false unless /[^01]/.match(s).nil?
  return false if s.empty?
  return false unless s.to_i(2) % 4 == 0
  true
end

raise "given the empty string it should return false" if binary_multiple_of_4?("")
raise "given the string '11' it should return false" if binary_multiple_of_4?("11")
raise "given the string '1sda0011' it should return false" if binary_multiple_of_4?("1sda1")
raise "given the string 'as10001100das' it should return false" if binary_multiple_of_4?("as10001100das")
raise "given the string '12504' it should return false" if binary_multiple_of_4?("12504")
raise "given the string 'hi_there' it should return false" if binary_multiple_of_4?("Hithere")
raise "given the string '1000' it should return true" unless binary_multiple_of_4?("1000")
raise "given the string '100' it should return true" unless binary_multiple_of_4?("100")

