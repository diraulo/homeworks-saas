class MoviesController < ApplicationController
  def initialize
      @all_ratings = Movie.all_ratings
      super
  end

  def show
    id = params[:id] # retrieve movie ID from URI route
    @movie = Movie.find(id) # look up movie by unique ID
    # will render app/views/movies/show.<extension> by default
  end

  def index

    redirect = false

    if params[:sort]
        @sorting = params[:sort]
    elsif session[:sort]
        @sorting = session[:sort]
        redirect = true
    end

    if params[:ratings]
        @ratings = params[:ratings]
    elsif session[:ratings]
        @ratings = session[:ratings]
        redirect = true
    else
        @all_ratings.each do |rat|
            (@ratings ||= { })[rat] = 1
        end
        redirect = true
    end

    if redirect
        redirect_to movies_path(:sort => @sorting, :ratings => @ratings)
    end

    Movie.find(:all, :order => @sorting ? @sorting : :id).each do |mv|
        if @ratings.keys.include? mv[:rating]
            (@movies ||= [ ]) << mv
        end
    end

    session[:sort]    = @sorting
    session[:ratings] = @ratings
    # @all_ratings = ['G','PG','PG-13','R']
    # @sort = params[:sort_by]
    # return @movies = Movie.order(params[:sort_by]).all unless params[:sort_by].nil?
    # @movies = Movie.all
  end

  def new
    # default: render 'new' template
  end

  def created
    @movie = Movie.create!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully created."
    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes!(params[:movie])
    flash[:notice] = "#{@movie.title} was successfully updated."
    redirect_to movie_path(@movie)
  end

  def destroy
    @movie = Movie.find(params[:id])
    @movie.destroy
    flash[:notice] = "Movie '#{@movie.title}' deleted."
    redirect_to movies_path
  end

  def sort_by_title
    @movies = Movie.all
    @movies.sort { |movie1, movie2| movie1<=>movie2 }
  end

end
