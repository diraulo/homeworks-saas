class RockPaperScissors

  # Exceptions this class can raise:
  class NoSuchStrategyError < StandardError ; end

  def self.winner(player1, player2)
    raise  NoSuchStrategyError, "Strategy must be one of R,P,S" unless player1[1].match(/R|P|S/i) && player2[1].match(/R|P|S/i)
    return player1 if player1[1] == player2[1]

    return player2 if player2[1].match(/P/i) && player1[1].match(/R/i)
    return player2 if player2[1].match(/R/i) && player1[1].match(/S/i)
    return player2 if player2[1].match(/S/i) && player1[1].match(/P/i)
    player1 # Player1 wins if all tests above fail
  end

  def self.tournament_winner(tournament)
    # Base case: We only have one game, return the winner
    return self.winner(tournament[0], tournament[1]) if tournament[0][0].is_a?(String)
    # Get winner of every game
    result = []
    tournament.each do |tourny|
      result << self.tournament_winner(tourny) if tourny.length >= 2 && tourny[0].is_a?(Array)
    end

    # Get winner among all the winners
    self.tournament_winner(result)
  end
end
