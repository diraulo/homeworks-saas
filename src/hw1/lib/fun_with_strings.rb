module FunWithStrings
  def palindrome?
    return self.downcase.gsub(/(\W|\d)/, "") == self.downcase.gsub(/(\W|\d)/, "").reverse
  end
  def count_words
    words = self.strip.split(/\W+/)  # Trim received string and split into words
    hash = Hash.new(0)               # Create empty hash
    words.each { |word| hash[word.downcase] += 1 }  # Counts every words occurrence
    hash
  end
  def anagram_groups
    return [] if self.empty?
    words = self.strip.split(/\W+/)
    words.group_by { |element| element.downcase.chars.sort }.values
  end
end

# make all the above functions available as instance methods on Strings:
class String
  include FunWithStrings
end
