class Class
  def attr_accessor_with_history(attr_name)
    attr_name = attr_name.to_s # make sure it's a string
    attr_reader attr_name # create the attribute's getter
    attr_reader attr_name+"_history" # create bar_history getter

    class_eval %Q{
      # getter
      def #{attr_name} ; @#{attr_name} ; end

      # setter
      def #{attr_name}=(value)
        @#{(attr_name+"_history")} ||= [nil]
        @#{(attr_name+"_history")} << @#{attr_name} unless @#{attr_name}.nil?
        @#{attr_name}=value
      end

      # attr_name+"_history"
      def #{(attr_name+"_history")} ; @#{(attr_name+"_history")} ; end
    }
  end
end
