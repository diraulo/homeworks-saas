# Add a declarative step here for populating the DB with movies.

Given /the following movies exist/ do |movies_table|
  movies_table.hashes.each do |movie|
    # each returned element will be a hash whose key is the table header.
    # you should arrange to add that movie to the database here.
    Movie.create!(movie)
  end
end

# Make sure that one string (regexp) occurs before or after another one
#   on the same page

Then /I should see "(.*)" before "(.*)"/ do |e1, e2|
  #  ensure that that e1 occurs before e2.
  #  page.body is the entire content of the page as a string.
  page.body.index(e1).should be < page.body.index(e2)
end

# Make it easier to express checking or unchecking several boxes at once
#  "When I uncheck the following ratings: PG, G, R"
#  "When I check the following ratings: G"

When /I (un)?check the following ratings: (.*)/ do |uncheck, rating_list|
  # HINT: use String#split to split up the rating_list, then
  #   iterate over the ratings and reuse the "When I check..." or
  #   "When I uncheck..." steps in lines 89-95 of web_steps.rb
  ratings = rating_list.split(",").map! { |e| e.strip }
  ratings.each do |rating|
    step %Q{I #{uncheck}check "ratings_#{rating}"}
  end
end

Then /I should (not )?see movies with ratings: (.*)/ do |should_not, rating_list|
  ratings = rating_list.split(",").map! { |e| e.strip }
  row_count_should_see = 0

  ratings.each { |rating| row_count_should_see += Movie.find_all_by_rating(rating).count }

  page.all("table#movies tbody tr").count.should == row_count_should_see
end

Then /I should see all the movies/ do
  # Make sure that all the movies in the app are visible in the table

  rows = page.all(:xpath, '//table/tbody/tr')
  rows.length.should == 10
  # page.all("table#movies tbody tr").count.should == 10 #Movie.count
  # ratings = page.all("form#ratings_form").map {|t| t.text}
  # puts ratings
  # all_ratings = ratings[0].gsub("Include ", "").split(" ")
  # row_count = 0
  #
  # all_ratings.each { |rating| row_count += Movie.find_all_by_rating(rating).count }
  #
  # rows = page.all("table#movies tbody tr td[1]").map {|t| t.text}
  # puts rows
  # rows.size.should == row_count
end
